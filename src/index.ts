
import { registerPlugin } from "@capacitor/core";

import type { KirillCodesPushPlugin } from "./definitions";

const KirillCodesPush = registerPlugin<KirillCodesPushPlugin>("KirillCodesPush", {
  async web ( ) {
    return import("./web").then(function getPushWeb ( module ) {
      return new module.KirillCodesPushWeb();
    });
  },
});

export { KirillCodesPush, KirillCodesPushPlugin };
