
// import type { ListenerCallback, PluginListenerHandle } from "@capacitor/core";

export interface KirillCodesPushPlugin {
  requestPermission ( ): Promise<{
    ok: boolean; message: string;
  }>;

  checkPermission ( ): Promise<{
    status: string;
  }>;
}
