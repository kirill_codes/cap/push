/* eslint-disable @typescript-eslint/ban-ts-comment */

import type { KirillCodesPushPlugin } from "./definitions";
import { WebPlugin } from "@capacitor/core";
// import type { ListenerCallback, PluginListenerHandle } from "@capacitor/core";

export class KirillCodesPushWeb extends WebPlugin implements KirillCodesPushPlugin {

  public async requestPermission ( ): Promise<{
    ok: boolean; message: string;
  }> {
    return { ok: false, message: "Not supported on web", };
  }

  public async checkPermission ( ): Promise<{
    status: string;
  }> {
    return { status: "Not supported on web", };
  }

}
