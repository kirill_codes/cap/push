//  KirillCodesPushPlugin.swift
//  Plugin

//  Created by Kirill

import Capacitor
import Foundation
import UIKit


@objc(KirillCodesPushPlugin)
public class KirillCodesPushPlugin: CAPPlugin {
    override public func load() {
      UIApplication.shared.registerForRemoteNotifications()
      UNUserNotificationCenter.current().requestAuthorization(options: [ .provisional, ]) { granted, error in
        if let error = error { print(error, "ERROR") }
      }
    }

    @objc func checkPermission (_ call: CAPPluginCall) {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            switch settings.authorizationStatus  {
                case .authorized:
                  call.resolve([ "granted": true,  ])
                case .provisional:
                  call.resolve([ "granted": true, "type": "provisional", ])
                case .ephemeral:
                  call.resolve([ "granted": true, "type": "ephemeral", ])
                case .denied:
                  call.resolve([ "granted": false, "type":  "explicit", ])
                case .notDetermined:
                  call.resolve([ "granted": false, ])
                @unknown default:
                  call.resolve([ "granted": false, ])
            }
        }
    }

    @objc func requestPermission (_ call: CAPPluginCall) {
        UNUserNotificationCenter.current().requestAuthorization(options: [ .alert, .sound, .badge, ]) { granted, error in
            if let error = error { call.resolve([ "ok": false, "message": error.localizedDescription ]) }

            call.resolve([ "ok": true, "message": "Push Notification permission granted" ])
        }
    }
}
