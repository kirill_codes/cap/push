//
//  KirillCodesPushPlugin.m
//  Plugin
//
//  Created by Kirill Chernik on 9/6/22.
//

#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

CAP_PLUGIN(KirillCodesPushPlugin, "KirillCodesPush",
    CAP_PLUGIN_METHOD(checkPermission, CAPPluginReturnPromise);
    CAP_PLUGIN_METHOD(requestPermission, CAPPluginReturnPromise);
)
