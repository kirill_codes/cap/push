
import type { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
  appId            : "com.kirill_codes.plugin.push",
  webDir           : "out/cap",
  bundledWebRuntime: false,
  loggingBehavior  : "debug",
  ios              : {
    scrollEnabled    : false,
    allowsLinkPreview: false,
  },
  android: {
    path: "android"
  }
};

export default config;
